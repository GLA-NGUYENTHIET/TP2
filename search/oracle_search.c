void oracle_search(
    int *Pre_A, int *A,
    int Pre_n, int n,
    int Pre_elem, int elem,
    int pathcrawler__retres__search)
{
  int res = pathcrawler__retres__search;
  int found[n];

  int i;

  for (i = 0; i < n; i++)
  {
    if (A[i] == elem)
    {
      found[i] = 1;
    }
    else
    {
      found[i] = 0;
    }
  }

  if (res == -1)
  {
    for (i = 0; i < n; i++)
    {
      if (found[i])
      {
        pathcrawler_verdict_failure();
      }
    }
    pathcrawler_verdict_success();
  }

  else
  {
    if (found[res])
    {
      pathcrawler_verdict_success();
    }
    else
    {
      pathcrawler_verdict_failure();
    }
  }
}
