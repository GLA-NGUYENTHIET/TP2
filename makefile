.PHONY: sqrt search sort

sqrt:
	cd $@; zip $@.zip *.c

search:
	cd $@; zip $@.zip *.c

sort:
	cd $@; zip $@.zip *.c


clean:
	find . -name "*.zip" -type f -delete
