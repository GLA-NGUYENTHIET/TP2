#include <stdlib.h>

int search(int *table, int elem, int *trouve, int l)
{
  int i;
  for (i = 0; i < l; i++)
  {
    if (table[i] == elem && !trouve[i])
    {
      trouve[i] = 1;
      return 1;
    }
  }
  return 0;
}

void oracle_sort(
    int *Pre_table, int *table,
    int Pre_l, int l)
{
  int i;
  for (i = 0; i < l - 1; i++)
  {
    if (table[i] > table[i+1])
    {
      pathcrawler_verdict_failure();
    }
  }

  int trouve[l];
  for (i = 0; i < l; i++)
  {
    trouve[i] = 0;
  }

  for (i = 0; i < l; i++)
  {
    if (!search(table, Pre_table[i], trouve, l))
    {
      pathcrawler_verdict_failure();
    }
  }

  pathcrawler_verdict_success();
  return;
}
