1)

A)
Ce chemin correspond à la condition de la boucle while toujours vrai

Il n'y a plus de chemin infaisables car les boucles ne sont plus infinies puisqu'on passe au maximum k = 5 fois dans la boucle

B)
Il y a plus de chemins car il y a une condition if en plus. Ce code effectue la boucle une 2e fois si la somme est encore inférieure

C)
Il y a un chemin en moins pour sqrt3.c parce que on verifie si sum < a au lieu de sum <= a 

D)
i ( la valeur de retour ) est tel que i² <= a <= (i+1)² si a est positif, et 0 sinon

E)
sqrt1.c marche
sqrt2.c marche
sqrt3.c marche
sqrt4.c ne marche pas. cela est du au fait que sum soit mis à jour après la vérificiation que sum <= a, cela peut augmenter sum alors qu'il n'y a pas lieu d'etre.
ainsi, i ne sera pas incrémenté de la bonne maniere il y aura une erreur

2)

B) Il y a autant de chemins entre la première et deuxième version, cependant les longueurs des chemins de la 1ere version sont distribués de manière equi probable alors que
les longueurs de la 2e version se concentre autour d'une valeur.

C) Tableau trié

D) 

search3 : l'erreur est que lorsqu'on a high - low = 1 est que elem est sur high, low va prendre la valeur high et la boucle s'arrete, on ne retourne donc pas le bon resultat,
car on ne regarde pas quand l'intervalle est de taille 0 ( un seul elem )

search4 : la recherche dans le nouvel intervalle n'est pas la bonne, si l'element est inférieure il faut chercher dans la moitié inférieure et non la moitié supérieure 

search5 : la valeur de retour quand l'element n'est pas la est -1, mais pas 0

E) des voyons ne sont pas vert?

3)

A) 

pre : l = len(table)
post : V i j, i < j -> t[i] <= t[j], V i, pre_t[i] se trouve max 1 fois dans t

B)

sort1 : on omet le 1er elem
sort2 : on n'entre pas dans la boucle
sort3 : ok
sort4 : il y a inversement lorsque t[i] < t[i+1] alors que ca devrait etre l'inverse
sort5 : ok
sort6 : on oublie de remonter le 2e plus petit élément